#!/usr/bin/env bash

eval $(aws ecr get-login --region ap-northeast-1)
docker push $AWS_ACCOUNT_ID.dkr.ecr.ap-northeast-1.amazonaws.com/ecr-apne1-hi820test-repo:$CIRCLE_SHA1
./ecs-deploy --cluster ecr-apne1-hi820-test --service-name ecr-apne1-hi820-test-webapp-service --image $AWS_ACCOUNT_ID.dkr.ecr.ap-northeast-1.amazonaws.com/ecr-apne1-hi820test-repo:$CIRCLE_SHA1
exit 0
